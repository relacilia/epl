-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10 Des 2016 pada 05.38
-- Versi Server: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mppl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_histori`
--

CREATE TABLE IF NOT EXISTS `detail_histori` (
  `id_histori` varchar(10) NOT NULL,
  `id_layanan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_histori`
--

-- INSERT INTO `detail_histori` (`id_histori`, `id_layanan`) VALUES
-- ('HSTO0001', 'LAYN0001'),
-- ('HSTO0001', 'LAYN0003'),
-- ('HSTO0001', 'LAYN0004'),
-- ('HSTO0002', 'LAYN0004'),
-- ('HSTO0003', 'LAYN0004'),
-- ('HSTO0004', 'LAYN0002'),
-- ('HSTO0005', 'LAYN0002'),
-- ('HSTO0006', 'LAYN0002'),
-- ('HSTO0007', 'LAYN0002'),
-- ('HSTO0009', 'LAYN0004'),
-- ('HSTO0010', 'LAYN0001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokter`
--

CREATE TABLE IF NOT EXISTS `dokter` (
  `id_dokter` varchar(10) NOT NULL,
  `nama_dokter` varchar(255) NOT NULL,
  `alamat_dokter` varchar(255) NOT NULL,
  `telp_dokter` varchar(20) NOT NULL,
  `flag_delete` char(1) NOT NULL DEFAULT '0',
  `id_jenisDokter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokter`
--

-- INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `alamat_dokter`, `telp_dokter`, `flag_delete`, `id_jenisDokter`) VALUES
-- ('DOKT0001', 'dr. Agus Saifullah', 'Perum. Jayanegara Blok A / 45, Surabaya', '08572461824', '0', 0),
-- ('DOKT0002', 'dr. Yusuf Maulana', 'JL. Gajahmada No.43 Surabaya', '08572658907', '0', 0),
-- ('DOKT0003', 'Rere', 'ddggg', '0828389748', '0', 0);

--
-- Trigger `dokter`
--
DELIMITER $$
CREATE TRIGGER `insert_dokter` BEFORE INSERT ON `dokter`
 FOR EACH ROW BEGIN
  INSERT INTO dokter_seq VALUES (NULL);
  SET NEW.id_dokter = CONCAT('DOKT', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokter_seq`
--

CREATE TABLE IF NOT EXISTS `dokter_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokter_seq`
--

-- INSERT INTO `dokter_seq` (`id`) VALUES
-- (1),
-- (2),
-- (3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'dokter', ''),
(4, 'resepsionis', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `histori`
--

CREATE TABLE IF NOT EXISTS `histori` (
  `id_histori` varchar(10) NOT NULL,
  `tanggal_histori` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `resep` text NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `histori`
--

-- INSERT INTO `histori` (`id_histori`, `tanggal_histori`, `resep`, `catatan`) VALUES
-- ('HSTO0001', '2015-03-08 11:30:27', '10 Amoxicilin', 'Pasien mengalami Flu'),
-- ('HSTO0002', '2015-03-08 12:59:56', '10 gr Paracetamol', 'Keluhan minggu lalu telah berkurang'),
-- ('HSTO0004', '2015-03-26 14:20:31', 'Paracetamol 4gr', 'Pasien mengalami Gegar Otak'),
-- ('HSTO0008', '2016-09-26 09:33:35', 'yiyofngdiofhi', 'sakit batuk'),
-- ('HSTO0009', '2016-09-27 03:51:01', 'opi', 'sakit demam'),
-- ('HSTO0010', '2016-12-09 13:46:12', 'hhhh', 'cgh');

--
-- Trigger `histori`
--
DELIMITER $$
CREATE TRIGGER `histori_insert` BEFORE INSERT ON `histori`
 FOR EACH ROW BEGIN
  INSERT INTO histori_seq VALUES (NULL);
  SET NEW.id_histori = CONCAT('HSTO', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `histori_seq`
--

CREATE TABLE IF NOT EXISTS `histori_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `histori_seq`
--

-- INSERT INTO `histori_seq` (`id`) VALUES
-- (1),
-- (2),
-- (3),
-- (4),
-- (5),
-- (6),
-- (7),
-- (8),
-- (9),
-- (10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_dokter`
--

CREATE TABLE IF NOT EXISTS `jenis_dokter` (
  `id_jenisDokter` int(11) NOT NULL,
  `jenis_dokter` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `layanan`
--

CREATE TABLE IF NOT EXISTS `layanan` (
  `id_layanan` varchar(10) NOT NULL,
  `nama_layanan` varchar(255) NOT NULL,
  `tarif_layanan` int(11) NOT NULL,
  `flag_delete` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `layanan`
--

INSERT INTO `layanan` (`id_layanan`, `nama_layanan`, `tarif_layanan`, `flag_delete`) VALUES
('LAYN0001', 'Periksa Mata', 100000, '0'),
('LAYN0002', 'Medical Check Up', 120000, '0'),
('LAYN0003', 'Pemeriksaan Rongga Mulut', 40000, '1'),
('LAYN0004', 'Konsultasi', 40000, '0');

--
-- Trigger `layanan`
--
DELIMITER $$
CREATE TRIGGER `layanan_insert` BEFORE INSERT ON `layanan`
 FOR EACH ROW BEGIN
  INSERT INTO layanan_seq VALUES (NULL);
  SET NEW.id_layanan = CONCAT('LAYN', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `layanan_seq`
--

CREATE TABLE IF NOT EXISTS `layanan_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `layanan_seq`
--

INSERT INTO `layanan_seq` (`id`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE IF NOT EXISTS `pasien` (
  `id_pasien` varchar(10) NOT NULL,
  `nama_pasien` varchar(255) NOT NULL,
  `alamat_pasien` varchar(255) NOT NULL,
  `telp_pasien` varchar(20) NOT NULL,
  `jk_pasien` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pasien`
--

-- INSERT INTO `pasien` (`id_pasien`, `nama_pasien`, `alamat_pasien`, `telp_pasien`, `jk_pasien`) VALUES
-- ('PASN0008', 'Dam', '', '', 'L'),
-- ('PASN0009', 'Re', '', '', 'P'),
-- ('PASN0010', 'Nu', '', '', 'L'),
-- ('PASN0011', 'trtt', 's', 's', 'P');

--
-- Trigger `pasien`
--
DELIMITER $$
CREATE TRIGGER `pasien_insert` BEFORE INSERT ON `pasien`
 FOR EACH ROW BEGIN
  INSERT INTO pasien_seq VALUES (NULL);
  SET NEW.id_pasien = CONCAT('PASN', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien_seq`
--

CREATE TABLE IF NOT EXISTS `pasien_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pasien_seq`
--

-- INSERT INTO `pasien_seq` (`id`) VALUES
-- (1),
-- (2),
-- (3),
-- (4),
-- (5),
-- (6),
-- (7),
-- (8),
-- (9),
-- (10),
-- (11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` varchar(10) NOT NULL,
  `tanggal_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pasien` varchar(10) NOT NULL,
  `id_dokter` varchar(10) NOT NULL,
  `id_histori` varchar(10) NOT NULL,
  `id_layanan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

-- INSERT INTO `transaksi` (`id_transaksi`, `tanggal_transaksi`, `id_pasien`, `id_dokter`, `id_histori`, `id_layanan`) VALUES
-- ('TRNS0009', '2016-09-25 14:46:49', 'PASN0010', 'DOKT0001', 'HSTO0010', '0'),
-- ('TRNS0010', '2016-09-26 09:28:57', 'PASN0008', 'DOKT0001', 'HSTO0008', '0'),
-- ('TRNS0011', '2016-09-27 03:50:03', 'PASN0008', 'DOKT0001', 'HSTO0009', '0'),
-- ('TRNS0012', '2016-11-08 04:40:57', 'PASN0009', 'DOKT0001', '', '0'),
-- ('TRNS0013', '2016-12-09 13:14:45', 'PASN0009', 'DOKT0001', '', '0'),
-- ('TRNS0014', '2016-12-09 13:29:01', 'PASN0009', 'DOKT0001', '', '0'),
-- ('TRNS0015', '2016-12-09 13:44:06', 'PASN0009', 'DOKT0001', '', '0'),
-- ('TRNS0016', '2016-12-09 13:46:59', 'PASN0008', 'DOKT0001', '', '0');

--
-- Trigger `transaksi`
--
DELIMITER $$
CREATE TRIGGER `transaksi_insert` BEFORE INSERT ON `transaksi`
 FOR EACH ROW BEGIN
  INSERT INTO transaksi_seq VALUES (NULL);
  SET NEW.id_transaksi = CONCAT('TRNS', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_seq`
--

CREATE TABLE IF NOT EXISTS `transaksi_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_seq`
--

-- INSERT INTO `transaksi_seq` (`id`) VALUES
-- (1),
-- (2),
-- (3),
-- (4),
-- (5),
-- (6),
-- (7),
-- (8),
-- (9),
-- (10),
-- (11),
-- (12),
-- (13),
-- (14),
-- (15),
-- (16);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1481343289, 1, 'Admin', 'istrator', 'ADMIN', '0'),
-- (2, '::1', 'dr. agus saifullah', '$2y$08$IvwDy.BE3E9QANMcY8b/v.p6jyryoS.tzbBq4G1TLW.GxKcmBrhY2', NULL, 'agus@gmail.com', NULL, NULL, NULL, NULL, 1425184901, 1481344297, 1, 'dr. Agus', 'Saifullah', 'Klinik', '081234124'),
(2, '127.0.0.1', 'wawan hendrawan', '$2y$08$Uf3NPMefmr73tqD1f9txNeqhAgsHD8peMtmd81HCm/oigXtI6Wz0a', NULL, 'wawan@gmail.com', NULL, NULL, NULL, NULL, 1425822828, 1481343596, 1, 'Wawan', 'Hendrawan', 'SIKLIK', '0812849430');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 4);
-- (6, 3, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `dokter_seq`
--
ALTER TABLE `dokter_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histori`
--
ALTER TABLE `histori`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `histori_seq`
--
ALTER TABLE `histori_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_dokter`
--
ALTER TABLE `jenis_dokter`
  ADD PRIMARY KEY (`id_jenisDokter`);

--
-- Indexes for table `layanan`
--
ALTER TABLE `layanan`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `layanan_seq`
--
ALTER TABLE `layanan_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `pasien_seq`
--
ALTER TABLE `pasien_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `transaksi_seq`
--
ALTER TABLE `transaksi_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokter_seq`
--
ALTER TABLE `dokter`
  MODIFY `id_dokter` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `dokter_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `histori_seq`
--
ALTER TABLE `histori_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `jenis_dokter`
--
ALTER TABLE `jenis_dokter`
  MODIFY `id_jenisDokter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `layanan_seq`
--
ALTER TABLE `layanan_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pasien_seq`
--
ALTER TABLE `pasien_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `transaksi_seq`
--
ALTER TABLE `transaksi_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
