<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dokter extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('dokter_model');
		$this->load->model('user_model');
	}
	//menampilkan halaman utama class dokter
	public function index()
	{
		$data['records'] = $this->dokter_model->get();
		$this->load->view('dokter/dokter_view', $data);
	}

	//menampilkan semua laporan dokter
	public function laporan()
	{
		$data['records'] = $this->dokter_model->get();
		$this->load->view('dokter/dokter_laporan', $data);
	}

	//mencetak laporan
	public function cetak_laporan()
	{
		$this->load->library('fpdf');
		$data['records'] = $this->dokter_model->get();
		$this->load->view('dokter/cetak_laporan', $data);
	}

	private function data_dokter()
	{
		$data_dokter = array(
						 'nama_dokter'   => $this->input->post('nama_dokter'),
						 'alamat_dokter' => $this->input->post('alamat_dokter'),
						 'telp_dokter'   => $this->input->post('telp_dokter')
			);
		return $data_dokter;
	}

	private function data_user() {
		$data_user = array(
			'ip_address' => '127.0.0.1',
			'username' 	 => $this->input->post('nama_dokter'),
			'password' 	 => password_hash($this->input->post('email'), PASSWORD_DEFAULT),
			'email' 	 => $this->input->post('email'),
			'created_on' => time(),
			'last_login' => time(),
			'active'	 => 1,
			'first_name' => 'User',
			'last_name'  => 'Biasa',
			'company'  	 => 'SIKLINIK',
			'company'  	=> $this->input->post('telp_dokter'),
		);
		return $data_user;
	}

	public function insert()
	{
		if($this->input->post())
		{
			$data_dokter = $this->data_dokter();
			$data_user 	 = $this->data_user();
			$this->dokter_model->insert($data_dokter);
			$this->user_model->insert($data_user);
			$user_id = $this->user_model->getiduserbyemail($this->input->post('email'))[0]["id"];
			
			$this->dokter_model->insertGroup(array('user_id' => $user_id, 'group_id' => 3));
			redirect('dokter');
		}
		else
		{
			$data['email'] = true;
			$this->load->view('dokter/kelola_dokter', $data);
		}
	}

	public function edit($id = NULL)
	{
		if($id == NULL) redirect('dokter','refresh');

		if($this->input->post())
		{
			$data_dokter = $this->data_dokter();
			$this->dokter_model->update($id, $data_dokter);

			redirect('dokter');
		}
		else
		{
			$data['dokter'] = $this->dokter_model->get( $id );
			$this->load->view('dokter/kelola_dokter', $data);
		}
	}

	public function delete($id = NULL)
	{
		if($id != NULL) $this->dokter_model->update($id, array('flag_delete' => '1'));

		redirect('dokter');
	}

	public function crud()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('pasien');
		$output = $crud->render();

		$this->load->view('pasien_view_1.php', $output);
	}

}
