<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url()?>">SI KLINIK</a> 
            </div>
            <div class="dropdown" style=" float: right; padding: 15px 50px 5px 50px; font-size: 16px;">
                  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <?php echo $this->session->userdata("username"); ?>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="<?php echo base_url('auth/change_password_page') ?>">Change Password</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url('auth/logout')?>">Logout</a></li>
                  </ul>
            </div>
</nav>   
<!-- /. NAV TOP  -->