<!DOCTYPE html>
<html>
	<head>
		<?php $this->load->view('layout/general_css'); ?>
	</head>
	<body>
		<div id="wrapper">
			<?php $this->load->view('layout/nav_top'); ?>
			<?php $this->load->view('layout/nav_side'); ?>
			<div id="page-wrapper">
				<div id="page-inner">

					<div class="row" style="padding-left:15px;">
						<h2 >Change Password</h2> 
						<span style="color:red"><?php echo (validation_errors()) ? validation_errors() : $this->session->flashdata('message'); ?> </span>
						<form id="form_regis" method="POST" action="<?php echo site_url('auth/change_password'); ?>"> 
	                    <div class="col-md-6">
	        				<p>Masukkan Password Lama </p>
	                        <input type="password" class="form-control" placeholder="old password" aria-describedby="basic-addon1" name="old">
	                        <p>Masukkan Password Baru </p>
	                        <input type="password" class="form-control" placeholder="new password" aria-describedby="basic-addon1" name="new">
	                        <p>Konfirmasi Password Lama </p>
	                        <input type="password" class="form-control" placeholder="confirm new password" aria-describedby="basic-addon1" name="new_confirm">
	                           <br>
	                        <input class="btn-flat" type="submit">
	                    </div>
					    </form>
	                </div>
	                <!-- /. ROW  -->

				</div>
			</div>
			
		</div>
		<?php $this->load->view('layout/general_js'); ?>
	</body>
</html>