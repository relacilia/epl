<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model
{
	public function insert($data = array())
	{
		$this->db->insert('users', $data);
	}

	public function update($id, $data = array())
	{
		$this->db->update('users', $data, array('id' => $id));
	}

	public function delete($id)
	{
		$this->db->delete('users', array('id' => $id));
	}
	public function getiduserbyemail($email) {
		return $this->db->get_where('users', array('email' => $email))->result_array();
	}
}
